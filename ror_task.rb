def find_ocean(map, x, y)
  temp = []
  temp << {x: x - 1, y: y} if x > 0
  temp << {x: x + 1, y: y} if map[x + 1]
  temp << {x: x, y: y - 1} if y > 0
  temp << {x: x, y: y + 1} if map[x][y + 1]

  if map[x][y] == 'W'
    map[x][y] = 'O'
    temp.each do |cell|
      if map[cell[:x]][cell[:y]] == 'W'
        find_ocean map, cell[:x], cell[:y]
      end
    end
  end
end
